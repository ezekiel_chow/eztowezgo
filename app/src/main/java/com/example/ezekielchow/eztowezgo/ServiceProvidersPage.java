package com.example.ezekielchow.eztowezgo;

import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.RequestTickle;
import com.android.volley.Response;
import com.android.volley.error.VolleyError;
import com.android.volley.request.GsonRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.android.volley.toolbox.VolleyTickle;
import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Random;

import com.example.ezekielchow.eztowezgo.PhotoPair;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ServiceProvidersPage extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback{

    private GoogleApiClient mGoogleApiClient;

    // Request code to use when launching the resolution activity
    private static final int REQUEST_RESOLVE_ERROR = 1001;
    // Unique tag for the error dialog fragment
    private static final String DIALOG_ERROR = "dialog_error";
    // Bool to track whether the app is already resolving an error
    private boolean mResolvingError = false;
    //Keep track of boolean when activity restarts (rotate screen)
    private static final String STATE_RESOLVING_ERROR = "resolving_error";

    //Getting current location
    private Location mLastLocation;
    private GoogleMap mMap;

    //selected map marker details
    String selectedMarkerLat;
    String selectedMarkerLong;
    String selectedMarkerName;
    String selectedMarkerRate;
    ArrayList<PhotoPair> photoReferences;
    PhotoPair photoPair;

    //for loading
    private BlackHoleView blackHoleView;
    private ProgressBarCircularIndeterminate progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_providers_page);

        //initialising
        photoReferences = new ArrayList<PhotoPair>();
        photoPair = new PhotoPair();
        selectedMarkerName = "";

        //initialize and disable black hole (touch events)
        blackHoleView = (BlackHoleView) findViewById(R.id.black_hole);
        blackHoleView.disable_touch(true);
        progressBar = (ProgressBarCircularIndeterminate)findViewById(R.id.loadingBar);
        progressBar.setVisibility(View.VISIBLE);

        //after this only get data
        buildGoogleApiClient();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.googleMapFragment);
        mapFragment.getMapAsync(this);
        mMap = mapFragment.getMap();

        //listener for marker
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                selectedMarkerLat = String.valueOf(marker.getPosition().latitude);
                selectedMarkerLong = String.valueOf(marker.getPosition().longitude);
                selectedMarkerName = marker.getTitle();
                String fullSnippet = marker.getSnippet();
                selectedMarkerRate = fullSnippet.substring(9,13);


                return false;
            }
        });

        //when activity restarts, recheck
        mResolvingError = savedInstanceState != null
                && savedInstanceState.getBoolean(STATE_RESOLVING_ERROR, false);


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(STATE_RESOLVING_ERROR, mResolvingError);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        if (mResolvingError) {
            // Already attempting to resolve an error.
            return;
        } else if (result.hasResolution()) {
            try {
                mResolvingError = true;
                result.startResolutionForResult(this, REQUEST_RESOLVE_ERROR);
            } catch (IntentSender.SendIntentException e) {
                // There was an error with the resolution intent. Try again.
                mGoogleApiClient.connect();
            }
        } else {
            // Show dialog using GooglePlayServicesUtil.getErrorDialog()
            showErrorDialog(result.getErrorCode());
            mResolvingError = true;
        }
    }

    /* Creates a dialog for an error message */
    private void showErrorDialog(int errorCode) {
        // Create a fragment for the error dialog
        ErrorDialogFragment dialogFragment = new ErrorDialogFragment();
        // Pass the error that should be displayed
        Bundle args = new Bundle();
        args.putInt(DIALOG_ERROR, errorCode);
        dialogFragment.setArguments(args);
        dialogFragment.show(dialogFragment.getFragmentManager(), "errordialog");
    }

    /* Called from ErrorDialogFragment when the dialog is dismissed. */
    public void onDialogDismissed() {
        mResolvingError = false;
    }

    /* A fragment to display an error dialog */
    public static class ErrorDialogFragment extends DialogFragment {
        public ErrorDialogFragment() { }

        @Override
        public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get the error code and retrieve the appropriate dialog
            int errorCode = this.getArguments().getInt(DIALOG_ERROR);
            return GooglePlayServicesUtil.getErrorDialog(errorCode, this.getActivity(), REQUEST_RESOLVE_ERROR);
        }

        @Override
        public void onDismiss(DialogInterface dialog) {
            ((ServiceProvidersPage)getActivity()).onDialogDismissed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == REQUEST_RESOLVE_ERROR) {
            mResolvingError = false;
            if (resultCode == RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mGoogleApiClient.isConnecting() &&
                        !mGoogleApiClient.isConnected()) {
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    public void onMapReady(GoogleMap map) {
        map.setMyLocationEnabled(true);
        map.addMarker(new MarkerOptions()
                .position(new LatLng(0,0))
                .title("Current Location"));
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null)
        {
            //move camera to current location
            Log.d("log latlng", String.valueOf(mLastLocation.getLatitude()));
            MapFragment fragment = (MapFragment)getFragmentManager().findFragmentById(R.id.googleMapFragment);
            mMap = fragment.getMap();
            mMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude())));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(11));

            //get workshops around current location
            //update map with nearby workshops
            String placeSearchURL = generatePlaceSearchAPI();
            requestDataFromAPI(placeSearchURL, "PlaceSearchAPI");
        }
    }

    private void requestDataFromAPI(String URLToUse, final String typeOfRequest)
    {
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URLToUse, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (typeOfRequest == "PlaceSearchAPI")
                {
                    populateMapWithWorkshops(response);

                    //enable button back
                    enableControls();
                }
                else if (typeOfRequest == "DirectionsAPI")
                {
                    setWorkshopValues(response);
                }
                else{}
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        mRequestQueue.add(stringRequest);
    }

    private String generatePlaceSearchAPI()
    {
        String basicURL = "https://maps.googleapis.com/maps/api/place/nearbysearch/";
        String outputType = "json";
        String APIKey = getString(R.string.googleAPIBrowserKey);
        String location = String.valueOf(mLastLocation.getLatitude()) + "," + String.valueOf(mLastLocation.getLongitude());
        String radius = "20000";
        String types = "car_repair";

        String completeURL = basicURL + outputType + "?" + "location=" + location + "&radius=" + radius + "&types=" + types + "&key=" + APIKey;
        Log.d("Place Search URL:", completeURL);
        return completeURL;
    }

    private String getDirectionsForPlace(String destinationLat, String destinationLong)
    {
        String basicURL = "https://maps.googleapis.com/maps/api/directions/";
        String outputType = "json";
        String APIKey = getString(R.string.googleAPIBrowserKey);
        String origin = String.valueOf(mLastLocation.getLatitude()) + "," + String.valueOf(mLastLocation.getLongitude());
        String destination = destinationLat + "," + destinationLong;

        String completeURL = basicURL + outputType + "?" + "origin=" + origin + "&destination=" + destination + "&key=" + APIKey;
        Log.d("Directions To Place:", completeURL);
        return completeURL;
    }

    private void populateMapWithWorkshops(String response)
    {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray resultsArray = jsonObject.getJSONArray("results");

            for (int i=0;i<resultsArray.length();i++)
            {
                JSONObject oneWorkshop = resultsArray.getJSONObject(i);
                String workshopName = oneWorkshop.getString("name");

                JSONObject geometry = oneWorkshop.getJSONObject("geometry");
                JSONObject location = geometry.getJSONObject("location");
                String latitude = location.getString("lat");
                String longitude = location.getString("lng");

                float leftLimit = 1F;
                float rightLimit = 5F;
                float generatedFloat = leftLimit + new Random().nextFloat() * (rightLimit - leftLimit);

                //safe photoreference in array
                if (!(oneWorkshop.has("photos")))
                {

                }else
                {
                    JSONArray photo = oneWorkshop.getJSONArray("photos");
                    JSONObject photoObj = photo.getJSONObject(0);

                    photoPair = new PhotoPair();
                    photoPair.setName(workshopName);
                    photoPair.setPhotoReference(photoObj.getString("photo_reference"));
                    photoReferences.add(photoPair);
                }

                mMap.addMarker(new MarkerOptions()
                .position(new LatLng(Double.valueOf(latitude), Double.valueOf(longitude)))
                .title(workshopName)
                .icon(BitmapDescriptorFactory.defaultMarker(120)))
                .setSnippet("\nRate: Rm" + String.format("%.2f", generatedFloat) + "/KM");
            }

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    private void setWorkshopValues(String response)
    {
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray routesArray = jsonObject.getJSONArray("routes");
            JSONObject routesObject = routesArray.getJSONObject(0);
            JSONArray legsArray = routesObject.getJSONArray("legs");
            JSONObject legsObject = legsArray.getJSONObject(0);
            JSONObject distanceObject = legsObject.getJSONObject("distance");
            JSONObject durationObject = legsObject.getJSONObject("duration");

            //go to next activity with extra data
            String selectedPlacePhotoReference = "";
            for (PhotoPair currentPlace : photoReferences)
            {
                Log.d("Name Selected", selectedMarkerName + "  " + currentPlace.getname());
                if (currentPlace.getname().equals(selectedMarkerName))
                {
                    selectedPlacePhotoReference = currentPlace.getPhotoReference();
                    Log.d("GOt La", selectedPlacePhotoReference);
                }
            }

            Log.d("Before", selectedPlacePhotoReference);
            Intent intent = new Intent(ServiceProvidersPage.this, ProviderDetails.class);
            intent.putExtra("distance", distanceObject.getString("value"));
            intent.putExtra("duration", durationObject.getString("value"));
            intent.putExtra("name", selectedMarkerName);
            intent.putExtra("photoReference", selectedPlacePhotoReference);
            intent.putExtra("rate", selectedMarkerRate);
            startActivity(intent);

        }catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public void selectWorkshop(View view)
    {
        if (selectedMarkerLong == null || selectedMarkerLat == null) //No workshop selected
        {
            Toast.makeText(ServiceProvidersPage.this, "Please select a workshop", Toast.LENGTH_SHORT).show();
        }
        else // there is workshop selected
        {
            blackHoleView.disable_touch(true);
            progressBar.setVisibility(View.VISIBLE);
            String directionsURL = getDirectionsForPlace(selectedMarkerLat, selectedMarkerLong);
            requestDataFromAPI(directionsURL, "DirectionsAPI");
        }
    }

    private void enableControls()
    {
        progressBar.setVisibility(View.INVISIBLE);
        blackHoleView.disable_touch(false);
    }
}
