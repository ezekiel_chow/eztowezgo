package com.example.ezekielchow.eztowezgo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.reflect.Array;


public class DetailsPage extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    private static String MY_SHARED_PREF = "EzTowEzGoSharedPref";
    private String vehicleProblem = "";
    private String detailsPageLog = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_page);

        //if got transaction, do rating
        SharedPreferences prefs = getSharedPreferences("ezTowSave", MODE_PRIVATE);
        String placeName = prefs.getString("placeName", null);
        if (placeName != null) {
            Intent intent = new Intent(this, Rate.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish(); // call this to finish the current activity
        }

        //populating vehicle problem spinner
        Spinner vehicleProblemSpinner = (Spinner)findViewById(R.id.vehicleProblemSpinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.vehiclesProblem, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        vehicleProblemSpinner.setAdapter(adapter);

        //vehicle problem spinner listener
        vehicleProblemSpinner.setOnItemSelectedListener(this);
    }

    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // An item was selected. You can retrieve the selected item using
        // parent.getItemAtPosition(pos)
        vehicleProblem = parent.getItemAtPosition(pos).toString();

    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
        vehicleProblem = "I Dont Know";
    }

    public void findTowingServiceClicked(View view)
    {
        if (checkAndSafeData())
        {
            Intent toServiceProviders = new Intent(this, ServiceProvidersPage.class);
            startActivity(toServiceProviders);
        }
        else{}
    }

    private boolean checkAndSafeData()
    {
        EditText numberPlate = (EditText)findViewById(R.id.numberPlateEditText);

        //check
        if (numberPlate.length() < 1)
        {
            Toast.makeText(this, "Please Enter Your Number Plate", Toast.LENGTH_SHORT).show();
            return false;
        }
        else
        {
            String numberPlateText = numberPlate.getText().toString();

            SharedPreferences.Editor editor = getSharedPreferences(MY_SHARED_PREF, MODE_PRIVATE).edit();
            editor.putString("vehicleProblem", vehicleProblem);
            editor.putString("numberPlate", numberPlateText);
            editor.commit();

            Log.d("log", "Putting stuff in to shared preferences");
            return true;
        }
    }

    public static Intent newFacebookIntent(PackageManager pm, String url) {
        Uri uri;
        String pageID = "371754496367715";
        try {
            pm.getPackageInfo("com.facebook.katana", 0);
            // http://stackoverflow.com/a/24547437/1048340
            uri = Uri.parse("fb://facewebmodal/f?href=" + url);
        } catch (PackageManager.NameNotFoundException e) {
            uri = Uri.parse("fb://facewebmodal/f?href=" + url);
        }
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public void openFBPage(View view)
    {
        Intent goToFB = newFacebookIntent(this.getPackageManager(), "https://www.facebook.com/eztowezgo");
        startActivity(goToFB);
    }
}
