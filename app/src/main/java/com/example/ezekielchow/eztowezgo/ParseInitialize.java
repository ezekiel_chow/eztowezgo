package com.example.ezekielchow.eztowezgo;

import android.app.Application;
import android.util.Log;

import com.parse.Parse;
import com.parse.ParseCrashReporting;

/**
 * Created by ezekielchow on 7/20/15.
 */
public class ParseInitialize extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        ParseCrashReporting.enable(this);
        Parse.enableLocalDatastore(this);
        Parse.initialize(this, "s9x4OvXQHmPJrv1xoJY9M3iwLC1TkI26AyfIdNlI", "TFfE8jBawsBqhsXFAEdUQ1GMlyhpd6eu4qzcZ7vd");

        Log.d("Parse", "Initializing");
    }
}
