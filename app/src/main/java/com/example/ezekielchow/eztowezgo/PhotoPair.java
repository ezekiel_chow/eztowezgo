package com.example.ezekielchow.eztowezgo;

/**
 * Created by ezekielchow on 6/24/15.
 */
public class PhotoPair { //class only for workshop with photos
    private String name = "";
    private String photoReference  = "";

    public PhotoPair(String name, String photoReference)
    {
        this.name = name;
        this.photoReference = photoReference;
    }

    public PhotoPair()
    {

    }

    public String getname() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhotoReference() {
        return photoReference;
    }

    public void setPhotoReference(String photoReference) {
        this.photoReference = photoReference;
    }

}
