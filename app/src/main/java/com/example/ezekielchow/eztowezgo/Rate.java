package com.example.ezekielchow.eztowezgo;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.gc.materialdesign.views.ButtonRectangle;
import com.parse.Parse;
import com.parse.ParseObject;

import org.w3c.dom.Text;

import java.util.concurrent.TimeUnit;

public class Rate extends AppCompatActivity {

    private int notificationID = 001;
    private String placeName, placeRating, serviceDate;
    private Button badBtn, neutralBtn, goodBtn;
    private ButtonRectangle confirmBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate);

        EditText commentEdittext = (EditText)findViewById(R.id.commentEditText);
        commentEdittext.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        //initialise variables
        confirmBtn = (ButtonRectangle)findViewById(R.id.confirmButton);
        placeRating = "2";

        SharedPreferences prefs = getSharedPreferences("ezTowSave", MODE_PRIVATE);
        placeName = prefs.getString("placeName", null);
        if (placeName != null) {
            serviceDate = prefs.getString("serviceDate", null);

            //populate textview with name and date
            TextView placeDateTextView = (TextView)findViewById(R.id.placeNameTextView);
            placeDateTextView.setText(placeName + " (" + serviceDate + ")");
        }
        else
        {
            Log.d("MUST FIX", "LOGIC ERROR laaa");
        }

        //initialise rating buttons
        badBtn = (Button)findViewById(R.id.badButton);
        neutralBtn = (Button)findViewById(R.id.okButton);
        goodBtn = (Button)findViewById(R.id.goodBtn);
        badBtn.setBackgroundResource(R.drawable.saddark);
        neutralBtn.setBackgroundResource(R.drawable.soso);
        goodBtn.setBackgroundResource(R.drawable.smiledark);

        //check intent empty
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("duration")) {
                String duration = extras.getString("duration");
                // duration exist, start timer

                confirmBtn.setEnabled(false);

                Double fullDuration = Double.valueOf(duration);
                long durationInMilliSecs = fullDuration.longValue() * 60000;

                new CountDownTimer(durationInMilliSecs, 1000) {

                    public void onTick(long millisUntilFinished) {
                        confirmBtn.setText(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + "." + (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));

                        NotificationManager mNotificationManager =
                                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


                        // Sets an ID for the notification, so it can be updated
                        NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(getApplicationContext())
                                .setContentTitle("EzTowEzGo")
                                .setContentText("Tow Arriving In: " + TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) + "." + (TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))) + " minutes")
                                .setSmallIcon(R.mipmap.ic_launcher);

                        mNotificationManager.notify(
                                notificationID,
                                mNotifyBuilder.build());
                    }

                    public void onFinish() {

                        confirmBtn.setText("Confirm");
                        confirmBtn.setEnabled(true);

                        finishNotification();

                        //play sound
                        MediaPlayer mp = MediaPlayer.create(getApplicationContext(), R.raw.dingding);
                        mp.start();

                        Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                        // Vibrate for 1000 milliseconds
                        v.vibrate(1000);
                    }
                }.start();

            }
        }
    }

    private void finishNotification()
    {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle("EzTowEzGo")
                        .setContentText("Tow Reached");
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, Rate.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(Rate.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(notificationID, mBuilder.build());
    }

    //dont let user go back
    @Override
    public void onBackPressed()
    {

    }

    public void ratingButtonClicked(View view)
    {
        switch (view.getId())
        {
            case R.id.badButton:
                placeRating = "1";
                badBtn.setBackgroundResource(R.drawable.sad);
                neutralBtn.setBackgroundResource(R.drawable.sosodark);
                goodBtn.setBackgroundResource(R.drawable.smiledark);
                break;

            case R.id.okButton:
                placeRating = "2";
                badBtn.setBackgroundResource(R.drawable.saddark);
                neutralBtn.setBackgroundResource(R.drawable.soso);
                goodBtn.setBackgroundResource(R.drawable.smiledark);
                break;

            case R.id.goodBtn:
                placeRating = "3";
                badBtn.setBackgroundResource(R.drawable.saddark);
                neutralBtn.setBackgroundResource(R.drawable.sosodark);
                goodBtn.setBackgroundResource(R.drawable.smile);
                break;
        }
    }

    public void confirmRate(View view)
    {
        //delete notification
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancelAll();

        saveRating();

        clearSaveData();

        Intent intent = new Intent(this, DetailsPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish(); // call this to finish the current activity
    }

    private void saveRating()
    {
        String commentCheck = "";
        EditText comment = (EditText)findViewById(R.id.commentEditText);
        if (comment.getText().toString() == "" || comment.getText().toString() == null)
        {
            commentCheck = "";
        }else
        {
            commentCheck = comment.getText().toString();
        }

        try {
            ParseObject placeRatingObj = new ParseObject("PlaceRating");
            placeRatingObj.put("placeName", placeName);
            placeRatingObj.put("placeComment", commentCheck);
            placeRatingObj.put("placeRating", placeRating);
            placeRatingObj.saveInBackground();
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        Log.d("HELP", placeName + commentCheck + placeRating);
    }

    private void clearSaveData()
    {
        SharedPreferences.Editor editor = getSharedPreferences("ezTowSave", MODE_PRIVATE).edit();

        editor.clear();
        editor.commit();
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
