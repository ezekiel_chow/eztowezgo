package com.example.ezekielchow.eztowezgo;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Created by ezekielchow on 7/10/15.
 */
public class BlackHoleView extends View {

    private boolean touch_disabled=true;

    public BlackHoleView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return touch_disabled;
    }

    public void disable_touch(boolean b) {
        touch_disabled=b;
    }
}