package com.example.ezekielchow.eztowezgo;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.cache.DiskLruBasedCache;
import com.android.volley.cache.plus.ImageCache;
import com.android.volley.cache.plus.SimpleImageLoader;
import com.android.volley.error.VolleyError;
import com.android.volley.request.ImageRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.gc.materialdesign.views.ProgressBarCircularIndeterminate;
import com.google.android.gms.location.places.Place;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class ProviderDetails extends AppCompatActivity {

    //average rating
    private double averageRating;

    private String durationString;
    private String name;

    //loading image
    private ProgressBarCircularIndeterminate progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_provider_details);

        Intent intent = getIntent();
        String duration = intent.getStringExtra("duration"); //in seconds
        String distance = intent.getStringExtra("distance"); //in meters
        String photoReference = intent.getStringExtra("photoReference"); //reference for photo, if empty no photo
        name = intent.getStringExtra("name");
        String rate = intent.getStringExtra("rate");

        //loading bar
        progressBar = (ProgressBarCircularIndeterminate)findViewById(R.id.loadingBar);
        progressBar.setVisibility(View.VISIBLE);

        //parse query
        getWorkshopAverageRating(name);

        //set textview values
        Double durationInMinutes = Double.valueOf(duration) / 60;
        Double distanceInKM = Double.valueOf(distance) / 1000;
        durationString = String.format("%.0f", durationInMinutes);
        String distanceString = String.format("%.2f", distanceInKM);
        TextView nameTextView = (TextView)findViewById(R.id.nameTextView);
        TextView durationTextView = (TextView)findViewById(R.id.minutesTextView);
        TextView priceTextView = (TextView)findViewById(R.id.priceValueTextView);
        Log.d("DEBUGGG", distanceString + " " + rate);
        Double totalPrice = Double.valueOf(distanceString) * Double.valueOf(rate);

        nameTextView.setText(name);
        durationTextView.setText("Approx " + durationString + " minutes");
        priceTextView.setText("Rm " + String.format("%.2f", totalPrice) + "(" + rate + "/KM)");

        CircleImageView circleImageView = (CircleImageView)findViewById(R.id.workshopImage);

        //get default photo
        if(photoReference.isEmpty())
        {
            progressBar.setVisibility(View.INVISIBLE);
            circleImageView.setImageResource(R.mipmap.ic_launcher);
        }
        else //get custom photo
        {
            setImageForView(photoReference, circleImageView);
        }
    }

    private void setImageForView(String photoReference, final CircleImageView circleImageView)
    {
        String basicURL = "https://maps.googleapis.com/maps/api/place/photo?";
        String APIKey = getString(R.string.googleAPIBrowserKey);
        int maxwidth = 400;

        String URLToUse = basicURL + "maxwidth=" + maxwidth + "&photoreference=" + photoReference + "&key=" + APIKey;
        Log.d("Image request", URLToUse);

        DiskLruBasedCache.ImageCacheParams cacheParams = new DiskLruBasedCache.ImageCacheParams(getApplicationContext(), "CacheDirectory");

        SimpleImageLoader mImageFetcher = new SimpleImageLoader(getApplicationContext(), cacheParams);
        mImageFetcher.setMaxImageSize(600);
        mImageFetcher.get(URLToUse, circleImageView);

    }

    public void confirmClicked(View view)
    {
        Intent intent = new Intent(this, Rate.class);
        intent.putExtra("duration", durationString);

        //get current date and pass it
        String date = new SimpleDateFormat("dd-MM-yyyy").format(new Date());

        SharedPreferences.Editor editor = getSharedPreferences("ezTowSave", MODE_PRIVATE).edit();
        editor.putString("placeName", name);
        editor.putString("serviceDate", date);
        editor.commit();


        startActivity(intent);
    }

    private void getWorkshopAverageRating(String workshopName)
    {
        Log.d("Parse workshop query:", ":" + workshopName + ":");
        ParseQuery<ParseObject> query = ParseQuery.getQuery("PlaceRating");
        query.whereEqualTo("placeName", workshopName);
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> ratingList, ParseException e) {
                if (e == null) {
                    Log.d("parse rating", "Retrieved " + ratingList.size() + " scores");

                    Double totalRate = 0.0;
                    for (ParseObject eachRate : ratingList) {

                        String rateNumber = eachRate.getString("placeRating");
                        Log.d("each rate", rateNumber);
                        totalRate += Double.valueOf(rateNumber);
                    }

                    averageRating = totalRate / ratingList.size();
                    updateAvrgRating();

                } else {
                    Log.d("parse rating", "Error: " + e.getMessage());
                }
            }
        });
    }

    private void updateAvrgRating()
    {
        TextView avrgRatingTxt = (TextView)findViewById(R.id.avgRatingTextView);

        if (averageRating == 0)
        {
            avrgRatingTxt.setText("No Ratings Yet");
        }
        else if (averageRating >= 0.1 && averageRating <= 0.6)
        {
            avrgRatingTxt.setText("Very Bad");
        }
        else if (averageRating >= 0.61 && averageRating <= 1.2)
        {
            avrgRatingTxt.setText("Bad");
        }
        else if (averageRating >= 1.21 && averageRating <= 1.8)
        {
            avrgRatingTxt.setText("Moderate");
        }
        else if (averageRating >= 1.81 && averageRating <= 2.4)
        {
            avrgRatingTxt.setText("Good");
        }
        else if (averageRating >= 2.41 && averageRating <= 3)
        {
            avrgRatingTxt.setText("Very Good");
        }
        else {
            avrgRatingTxt.setText("No Ratings Yet");
        }
    }
}
